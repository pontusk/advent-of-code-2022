def parse(data):
    map = {}
    for y, line in enumerate(data.strip().split("\n")):
        for x, char in enumerate(line):
            map[(x, y)] = char

    return map


def find_surrounding(map, point):
    points = map.keys()
    x, y = point

    surrounding = []

    top = (0, (x, y - 1))
    right = (1, (x + 1, y))
    bottom = (2, (x, y + 1))
    left = (3, (x - 1, y))

    if (top[1] in points):
        surrounding.append(top)
    if (right[1] in points):
        surrounding.append(right)
    if (bottom[1] in points):
        surrounding.append(bottom)
    if (left[1] in points):
        surrounding.append(left)

    return surrounding


def find_start(map):
    for key in map.keys():
        if map[key] == "S":
            return key


def plot_map(map):
    max_y = max(y for _, y in map.keys())
    max_x = max(x for x, _ in map.keys())

    print("")
    for y in range(0, max_y + 1):
        for x in range(0, max_x + 1):
            if (x, y) in map:
                print(map[(x, y)], end="")
            else:
                print(".", end="")
        print("")
    print("")


def solve(data):
    original = parse(data)
    map = original
    start = find_start(map)
    point = start
    elevation = 97 if map[point] == "S" else ord(map[point])
    path = []

    for _ in range(0, 20):
        surrounding = find_surrounding(map, point)
        for ip in surrounding:
            (i, p) = ip
            e = 122 if map[p] == "E" else ord(map[p])
            if e <= elevation + 1 and p not in path:
                if i == 0:
                    map[point] = "^"
                if i == 1:
                    map[point] = ">"
                if i == 2:
                    map[point] = "v"
                if i == 3:
                    map[point] = "<"
                path.append(point)
                point = p
                break


        plot_map(map)
