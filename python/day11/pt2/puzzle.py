import re
from collections import deque


def parse(data):
    paragraphs = data.split("\n\n")

    monkeys = []

    for paragraph in paragraphs:
        items = []
        first = None
        second = None
        operation = None
        divisable_by = None
        if_true = None
        if_false = None
        for line in paragraph.strip().split("\n"):
            if re.match("^Monkey", line):
                continue

            if re.match("^\\s*Starting items", line):
                [key, value] = line.split(": ")
                items = [int(x) for x in value.split(", ")]
            if re.match("^\\s*Operation", line):
                [first, operation, second] = re.findall(
                    "old|\\d+|\\*|\\+", line)
                first = first if first == "old" else int(first)
                second = second if second == "old" else int(second)
                operation = operation
            if re.match("^\\s*Test", line):
                [divisable_by] = re.findall("\\d+", line)
                divisable_by = int(divisable_by)
            if re.match("^\\s*If true", line):
                [if_true] = re.findall("\\d+", line)
                if_true = int(if_true)
            if re.match("^\\s*If false", line):
                [if_false] = re.findall("\\d+", line)
                if_false = int(if_false)

        monkeys.append({
            "inspected_items": 0,
            "items": deque(items),
            "first": first,
            "second": second,
            "operation": operation,
            "divisable_by": divisable_by,
            "if_true": if_true,
            "if_false": if_false})

    return monkeys


def solve(data):
    monkeys = parse(data)

    big_mod = 1
    for monkey in monkeys:
        big_mod *= monkey["divisable_by"]

    def turn(monkey):
        nonlocal monkeys
        [_, items, first, second, operation, divisable_by,
            if_true, if_false] = monkeys[monkey].values()
        while len(items) > 0:
            monkeys[monkey]["inspected_items"] += 1
            item = items.popleft()
            _first = item if first == "old" else first
            _second = item if second == "old" else second
            worry_level = _first * _second if operation == "*" else _first + _second
            worry_level = worry_level % big_mod
            test = worry_level % divisable_by == 0
            if if_true is not None and if_false is not None:
                if test:
                    monkeys[int(if_true)]["items"].append(worry_level)
                else:
                    monkeys[int(if_false)]["items"].append(worry_level)

    rounds = 10000

    for round in range(0, rounds):
        for monkey in range(0, len(monkeys)):
            turn(monkey)

    inspection_times = [m["inspected_items"] for m in monkeys]
    inspection_times.sort()
    largest = inspection_times.pop()
    second_largest = inspection_times.pop()
    monkey_business = largest * second_largest

    return monkey_business
