import unittest
import io
import os
import contextlib
import puzzle

with io.open(
    os.path.join(os.path.dirname(__file__), "..", "input"),
    "r",
    encoding="utf8"
) as f:
    data = f.read()


class TestScript(unittest.TestCase):
    def test_pass(self) -> None:
        test_answer = puzzle.solve("""
Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1
""")
        self.assertEqual(2713310158, test_answer)

        with open(os.devnull, "w") as devnull:
            with contextlib.redirect_stdout(devnull):
                answer = puzzle.solve(data)

        print(f"Final answer: {answer}\n")


if __name__ == "__main__":
    unittest.main()
