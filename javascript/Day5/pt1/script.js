const fs = require("fs");
let data = fs.readFileSync("../input", { encoding: "utf8" });

data = data.split("\n\n");

data = {
  cargo: parseCargo(data[0]),
  instructions: parseInstructions(data[1]),
};
function parseCargo(input) {
  let rows = input.split("\n");
  let cargo = [];
  rows = rows.map((it) => it.match(/(\s{3,4}|\[.\])/g));
  rows = rows.filter((it) => {
    return it.some((it) => /\w/.test(it));
  });

  for (let i = 0, len = rows.length; i < len; i++) {
    let row = rows[i];
    for (let j = 0, len = row.length; j < len; j++) {
      let cur = row[j];
      if (!/^\s+$/.test(cur)) {
        if (!cargo?.[j]) cargo[j] = [];
        cur = cur.replace(/\[|\]/g, "");
        cargo[j]?.unshift(cur);
      }
    }
  }
  return cargo;
}

function parseInstructions(input) {
  let rows = input.split("\n");
  rows = rows.map((it) => it.match(/\d+/g));
  return rows.reduce((acc, cur) => {
    if (cur) {
      const [move, from, to] = cur;
      acc.push({ move, from: from - 1, to: to - 1 });
    }
    return acc;
  }, []);
}

data.instructions.forEach((it) => {
  for (let i = 0, len = it.move; i < len; i++) {
    let toPush = data.cargo[it.from].pop();
    if (toPush) {
      data.cargo[it.to]?.push(toPush);
    }
  }
});

let answer = data.cargo.reduce((acc, cur) => {
  let letter = cur[cur.length - 1];
  if (letter) acc += letter;
  return acc;
}, "");

console.log(answer);
