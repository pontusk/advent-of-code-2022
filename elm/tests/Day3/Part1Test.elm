module Day3.Part1Test exposing (..)

import Array exposing (..)
import Days.Day03.Page exposing (..)
import Days.Day03.Inputs exposing (..)
import Expect exposing (..)
import Set exposing (..)
import Test exposing (..)


suite : Test
suite =
    describe "Day 3 Part 1"
        [ describe "parseInput"
            [ test "Parses the imput" <|
                \_ ->
                    smallInput
                        |> parseInput
                        |> Expect.equal
                            [ [ Set.fromList [ 'v', 'J', 'r', 'w' ]
                              , Set.fromList [ 'v', 'T', 't', 'k' ]
                              ]
                            , [ Set.fromList [ 'j', 'q', 'H', 'R', 'N' ]
                              , Set.fromList [ 'q', 'Q', 'J', 'F', 'z' ]
                              ]
                            ]
            ]
        , describe "Answer"
            [ test "Gives the right answer" <|
                \_ ->
                    finalInput |> solvePart1 |> Expect.equal 8123
            ]
        ]
