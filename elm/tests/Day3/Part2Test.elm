module Day3.Part2Test exposing (..)

import Array exposing (..)
import Days.Day03.Page exposing (..)
import Days.Day03.Inputs exposing (..)
import Expect exposing (..)
import Set exposing (..)
import Test exposing (..)


suite : Test
suite =
    describe "Day 3 Part 2"
        [ describe "Answer"
            [ test "Gives the right answer" <|
                \_ ->
                    finalInput |> solvePart2 |> Expect.equal 2620
            ]
        ]
