module Day6.Part2Test exposing (..)

import Array exposing (..)
import Days.Day6.Page exposing (..)
import Days.Day6.Inputs exposing (..)
import Expect exposing (..)
import Set exposing (..)
import Test exposing (..)


suite : Test
suite =
    describe "Day 6 Part 2"
        [  describe "Answer"
            [ test "Gives the right answer" <|
                \_ ->
                    finalInput |> solvePart2 |> Expect.equal "2950"
            ]
        ]
