module Day2.Part1Test exposing (..)

import Array exposing (..)
import Days.Day02.Page exposing (..)
import Days.Day02.Inputs exposing (..)
import Expect exposing (..)
import Test exposing (..)



-- A Y
-- B X
-- C Z


suite : Test
suite =
    describe "Day2 Part1"
        [ describe "parseInput"
            [ test "It parses input" <|
                \_ ->
                    input |> parseInput |> Expect.equal [ [ 'A', 'Y' ], [ 'B', 'X' ], [ 'C', 'Z' ] ]
            ]
        , describe "Answer"
            [ test "Get the answer" <|
                \_ ->
                    finalInput |> solvePart1 |> Expect.equal 11475
            ]
        ]
