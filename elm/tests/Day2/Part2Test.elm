module Day2.Part2Test exposing (..)

import Array exposing (..)
import Days.Day02.Page exposing (..)
import Days.Day02.Inputs exposing (..)
import Expect exposing (..)
import Test exposing (..)



-- A Y
-- B X
-- C Z


suite : Test
suite =
    describe "Day2 Part1"
        [ describe "Answer"
            [ test "Get the answer" <|
                \_ ->
                    finalInput |> solvePart2 |> Expect.equal 16862
            ]
        ]
