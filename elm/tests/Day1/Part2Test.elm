module Day1.Part2Test exposing (..)

import Array exposing (..)
import Days.Day01.Page exposing (..)
import Days.Day01.Inputs exposing (..)
import Expect exposing (..)
import Test exposing (..)


suite : Test
suite =
    describe "Day2 Part2"
        [ describe "Answer" [ 
            test "Get the answer" <| \_ -> 
                finalInput |> solvePart2 |> Expect.equal 210367
            ]
        ]
