module Day1.Part1Test exposing (..)

import Array exposing (..)
import Days.Day01.Inputs exposing (..)
import Days.Day01.Page exposing (..)
import Expect exposing (..)
import Test exposing (..)


suite : Test
suite =
    describe "Day1 Part1"
        [ describe "parseInput"
            [ test "Parse the input correctly" <|
                \_ ->
                    smallInput
                        |> parseInput
                        |> Expect.equal [ Just 2000, Just 3000, Nothing, Just 4000 ]
            ]
        , describe "run"
            [ test "Creates the model" <|
                \_ ->
                    smallInput
                        |> parseInput
                        |> calculateCalories
                        |> Expect.equal ( Array.fromList [ 5000, 4000 ], 1 )
            ]
        , describe "Answer"
            [ test "Get the answer" <|
                \_ ->
                    input |> solvePart1 |> Expect.equal 24000
            ]
        ]
