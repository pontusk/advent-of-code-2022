module Day4.Part1Test exposing (..)

import Array exposing (..)
import Days.Day04.Page exposing (..)
import Days.Day04.Inputs exposing (..)
import Expect exposing (..)
import Set exposing (..)
import Test exposing (..)


suite : Test
suite =
    describe "Day 4 Part 1"
        [  describe "Answer"
            [ test "Gives the right answer" <|
                \_ ->
                    finalInput |> solvePart1 |> Expect.equal 503
            ]
        ]
