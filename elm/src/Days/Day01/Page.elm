module Days.Day01.Page exposing (..)

import Array exposing (Array)
import Days.Day01.Inputs exposing (..)
import Html exposing (Html)
import Utils exposing (..)


parseInput : String -> List (Maybe Int)
parseInput input =
    input
        |> String.trim
        |> String.lines
        |> List.map String.toInt


calculateCalories : List (Maybe Int) -> ( Array Int, Int )
calculateCalories input =
    input
        |> List.foldl
            (\cur acc ->
                let
                    ( elves, index ) =
                        acc
                in
                case cur of
                    Just n ->
                        ( updateElves index n elves, index )

                    Nothing ->
                        ( elves, index + 1 )
            )
            ( Array.fromList [], 0 )


updateElves : Int -> Int -> Array Int -> Array Int
updateElves index n elves =
    let
        cur =
            Array.get index elves
    in
    case cur of
        Just a ->
            Array.set index (a + n) elves

        Nothing ->
            Array.push n elves


solvePart1 : String -> Int
solvePart1 input =
    input
        |> parseInput
        |> calculateCalories
        |> Tuple.first
        |> Array.toList
        |> List.maximum
        |> Maybe.withDefault 0


solvePart2 : String -> Int
solvePart2 input =
    input
        |> parseInput
        |> calculateCalories
        |> Tuple.first
        |> Array.toList
        |> List.sort
        |> List.reverse
        |> List.take 3
        |> List.sum


part1 : Html msg
part1 =
    finalInput
        |> solvePart1
        |> Debug.toString
        |> Html.text


part2 : Html msg
part2 =
    finalInput
        |> solvePart2
        |> Debug.toString
        |> Html.text
