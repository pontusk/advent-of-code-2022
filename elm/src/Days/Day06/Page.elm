module Days.Day06.Page exposing (..)

import Array exposing (Array)
import Days.Day06.Inputs exposing (finalInput)
import Html exposing (Html)
import List.Split exposing (..)
import Set exposing (Set)
import Utils exposing (..)


type alias Model =
    { signal : Array Char
    , index : Int
    , messageLength : Int
    }


initialModel : Model
initialModel =
    { signal = Array.empty, index = 0, messageLength = 0 }


parseInput : String -> List Char
parseInput input =
    input
        |> String.trim
        |> String.toList


solvePart1 : String -> Int
solvePart1 input =
    let
        parsedInput : List Char
        parsedInput =
            input |> parseInput

        signal : Array Char
        signal =
            parsedInput |> Array.fromList
    in
    parsedInput
        |> List.foldl subRoutine { initialModel | signal = signal, messageLength = 4 }
        |> .index


solvePart2 : String -> Int
solvePart2 input =
    let
        parsedInput : List Char
        parsedInput =
            input |> parseInput

        signal : Array Char
        signal =
            parsedInput |> Array.fromList
    in
    parsedInput
        |> List.foldl subRoutine { initialModel | signal = signal, messageLength = 14 }
        |> .index


subRoutine : Char -> Model -> Model
subRoutine _ model =
    let
        start : Int
        start =
            max 0 (model.index - model.messageLength)

        end : Int
        end =
            model.index

        sequence : Array Char
        sequence =
            Array.slice start end model.signal

        sequenceSet : Set Char
        sequenceSet =
            sequence |> Array.toList |> Set.fromList

        isSeqUniqueAndLongEnough : Bool
        isSeqUniqueAndLongEnough =
            Set.size sequenceSet
                == Array.length sequence
                && Array.length sequence
                == model.messageLength
    in
    if isSeqUniqueAndLongEnough then
        model

    else
        { model | index = model.index + 1 }


part1 : Html msg
part1 =
    finalInput
        |> solvePart1
        |> Debug.toString
        |> Html.text


part2 : Html msg
part2 =
    finalInput
        |> solvePart2
        |> Debug.toString
        |> Html.text
