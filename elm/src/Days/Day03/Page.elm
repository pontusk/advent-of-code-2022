module Days.Day03.Page exposing (..)

import Days.Day03.Inputs exposing (finalInput)
import Html exposing (Html)
import List.Split exposing (..)
import Set exposing (Set)
import Utils exposing (..)


parseInput : String -> List (List (Set Char))
parseInput input =
    input
        |> String.trim
        |> String.lines
        |> List.map
            (\rucksack ->
                let
                    half =
                        rucksack
                            |> String.length
                            |> divideBy 2
                            |> floor

                    compartment1 =
                        rucksack
                            |> String.slice 0 half
                            |> String.toList
                            |> Set.fromList

                    compartment2 =
                        rucksack
                            |> String.slice half (String.length rucksack)
                            |> String.toList
                            |> Set.fromList
                in
                [ compartment1, compartment2 ]
            )


solvePart1 : String -> Int
solvePart1 input =
    input
        |> parseInput
        |> List.map compareCompartments
        |> List.map toCharCode
        |> List.sum


solvePart2 : String -> Int
solvePart2 input =
    input
        |> String.trim
        |> String.lines
        |> List.map
            (\rucksack ->
                rucksack
                    |> String.toList
                    |> Set.fromList
            )
        |> List.Split.chunksOfLeft 3
        |> List.map compareElfGroups
        |> List.map toCharCode
        |> List.sum


compareCompartments : List (Set Char) -> Set Char
compareCompartments rucksack =
    case rucksack of
        compartment1 :: compartment2 :: [] ->
            compartment1 |> Set.intersect compartment2

        _ ->
            Set.empty


compareElfGroups : List (Set Char) -> Set Char
compareElfGroups group =
    case group of
        elf1 :: elf2 :: elf3 :: [] ->
            let
                first2 =
                    elf1 |> Set.intersect elf2
            in
            first2 |> Set.intersect elf3

        _ ->
            Set.empty


toCharCode : Set Char -> Int
toCharCode charSet =
    let
        char =
            charSet |> Set.toList |> List.head
    in
    case char of
        Just c ->
            case c of
                'a' ->
                    1

                'b' ->
                    2

                'c' ->
                    3

                'd' ->
                    4

                'e' ->
                    5

                'f' ->
                    6

                'g' ->
                    7

                'h' ->
                    8

                'i' ->
                    9

                'j' ->
                    10

                'k' ->
                    11

                'l' ->
                    12

                'm' ->
                    13

                'n' ->
                    14

                'o' ->
                    15

                'p' ->
                    16

                'q' ->
                    17

                'r' ->
                    18

                's' ->
                    19

                't' ->
                    20

                'u' ->
                    21

                'v' ->
                    22

                'w' ->
                    23

                'x' ->
                    24

                'y' ->
                    25

                'z' ->
                    26

                'A' ->
                    27

                'B' ->
                    28

                'C' ->
                    29

                'D' ->
                    30

                'E' ->
                    31

                'F' ->
                    32

                'G' ->
                    33

                'H' ->
                    34

                'I' ->
                    35

                'J' ->
                    36

                'K' ->
                    37

                'L' ->
                    38

                'M' ->
                    39

                'N' ->
                    40

                'O' ->
                    41

                'P' ->
                    42

                'Q' ->
                    43

                'R' ->
                    44

                'S' ->
                    45

                'T' ->
                    46

                'U' ->
                    47

                'V' ->
                    48

                'W' ->
                    49

                'X' ->
                    50

                'Y' ->
                    51

                'Z' ->
                    52

                _ ->
                    0

        Nothing ->
            0


part1 : Html msg
part1 =
    finalInput
        |> solvePart1
        |> Debug.toString
        |> Html.text


part2 : Html msg
part2 =
    finalInput
        |> solvePart2
        |> Debug.toString
        |> Html.text
