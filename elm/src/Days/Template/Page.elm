module Days.Template.Page exposing (..)

import Html exposing (Html)
import Utils exposing (..)


parseInput : String -> List String
parseInput input =
    input
        |> String.trim
        |> String.lines


solvePart1 : String -> List String
solvePart1 input =
    input
        |> parseInput


solvePart2 : String -> List String
solvePart2 input =
    input
        |> parseInput


part1 : String -> Html msg
part1 input =
    input
        |> solvePart1
        |> Debug.toString
        |> Html.text


part2 : String -> Html msg
part2 input =
    input
        |> solvePart2
        |> Debug.toString
        |> Html.text
