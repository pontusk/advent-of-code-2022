module Days.Day10.Page exposing (..)

import Array exposing (Array)
import Days.Day10.Inputs exposing (finalInput, input, smallInput)
import Html exposing (Html, div, p, text)
import Html.Attributes exposing (style)
import Utils exposing (..)


type Instruction
    = Noop
    | AddX Int


type alias Ctr =
    List (List Char)


type alias Model =
    { instructions : Array Instruction
    , index : Int
    , cycle : Int
    , x : Int
    , signalStrength : Int
    , ctr : Ctr
    }


initialModel : Model
initialModel =
    { instructions = Array.fromList []
    , index = 0
    , cycle = 0
    , x = 1
    , signalStrength = 0
    , ctr = []
    }


parseInput : String -> Model
parseInput input =
    input
        |> String.trim
        |> String.lines
        |> List.filterMap
            (\line ->
                line
                    |> String.words
                    |> (\words ->
                            case words of
                                "noop" :: [] ->
                                    Just Noop

                                "addx" :: v :: [] ->
                                    Just
                                        (AddX
                                            (Maybe.withDefault 0
                                                (String.toInt v)
                                            )
                                        )

                                _ ->
                                    Nothing
                       )
            )
        |> (\instructions ->
                { initialModel | instructions = Array.fromList instructions }
           )


executeInstructions : Model -> Model
executeInstructions model =
    model.instructions
        |> Array.toList
        |> List.foldl
            (\_ acc ->
                step acc
            )
            model


step : Model -> Model
step model =
    let
        maybeInstruction : Maybe Instruction
        maybeInstruction =
            Array.get model.index model.instructions
    in
    case maybeInstruction of
        Just instruction ->
            let
                newModel : Model
                newModel =
                    runCycles instruction model
            in
            { newModel | index = model.index + 1 }

        Nothing ->
            model


runCycles : Instruction -> Model -> Model
runCycles instruction model =
    case instruction of
        AddX v ->
            model
                |> runCycle Nothing
                |> runCycle (Just v)

        Noop ->
            model
                |> runCycle Nothing


runCycle : Maybe Int -> Model -> Model
runCycle maybeV model =
    let
        newCycle : Int
        newCycle =
            model.cycle + 1

        newSignalStrength : Int
        newSignalStrength =
            if newCycle == 20 || (modBy 40 (newCycle - 20) == 0) then
                model.signalStrength + (model.x * newCycle)

            else
                model.signalStrength

        newCtr : Ctr
        newCtr =
            drawPixels model
    in
    case maybeV of
        Just v ->
            { model
                | x = model.x + v
                , cycle = newCycle
                , signalStrength = newSignalStrength
                , ctr = newCtr
            }

        Nothing ->
            { model
                | cycle = newCycle
                , signalStrength = newSignalStrength
                , ctr = newCtr
            }


drawPixels : Model -> Ctr
drawPixels model =
    let
        sprite : Int
        sprite =
            modBy 40 model.x

        isLit =
            List.any (\x -> x == modBy 40 model.cycle)
                [ sprite - 1, sprite, sprite + 1 ]

        pixel : Char
        pixel =
            if isLit then
                '#'

            else
                '.'

        latestRow : List Char
        latestRow =
            Maybe.withDefault [] (List.head model.ctr)

        isLatestRowFull : Bool
        isLatestRowFull =
            List.length latestRow == 40
    in
    if isLatestRowFull then
        [ pixel ] :: model.ctr

    else
        (pixel :: latestRow) :: Maybe.withDefault [] (List.tail model.ctr)


solvePart1 : String -> Int
solvePart1 input =
    input
        |> parseInput
        |> executeInstructions
        |> .signalStrength


solvePart2 : String -> Ctr
solvePart2 input =
    input
        |> parseInput
        |> executeInstructions
        |> .ctr
        |> List.map List.reverse
        |> List.reverse

part1 : Html msg
part1 =
    finalInput
        |> solvePart1
        |> Debug.toString
        |> Html.text


part2 : Html msg
part2 =
    finalInput
        |> solvePart2
        |> (\x ->
                div [] (List.map viewCtr x)
           )


viewCtr : List Char -> Html msg
viewCtr line =
    p [ style "line-height" "0.8", style "margin" "0" ] [ text (String.fromList line) ]
