module Days.Day11.Page exposing (..)

import Days.Day11.Inputs exposing (input)
import Dict exposing (Dict)
import Html exposing (Html, table, td, text, th, tr)
import Html.Attributes exposing (style)
import Utils exposing (..)


type Operation
    = Multiplication
    | Addition


type Operand
    = Old
    | New Int


type alias Monkey =
    { items : List Int
    , itemsInspected : Int
    , operation : Operation
    , left : Operand
    , right : Operand
    , divisibleBy : Int
    , ifTrue : Int
    , ifFalse : Int
    }


initialMonkey : Monkey
initialMonkey =
    { items = []
    , itemsInspected = 0
    , operation = Addition
    , left = Old
    , right = New 0
    , divisibleBy = 1
    , ifTrue = 0
    , ifFalse = 0
    }


parseInput : String -> Dict Int Monkey
parseInput input =
    input
        |> String.trim
        |> String.split "\n\n"
        |> List.indexedMap makeMonkey
        |> Dict.fromList


makeMonkey : Int -> String -> ( Int, Monkey )
makeMonkey index str =
    str
        |> String.lines
        |> List.map
            (\x ->
                x |> String.split ":" |> List.map String.trim
            )
        |> List.foldl parseLine initialMonkey
        |> (\x -> ( index, x ))


parseLine : List String -> Monkey -> Monkey
parseLine list monkey =
    case list of
        "Starting items" :: rest ->
            let
                items : List Int
                items =
                    rest
                        |> List.head
                        |> Maybe.withDefault ""
                        |> String.split ","
                        |> List.map
                            (\x ->
                                x
                                    |> String.trim
                                    |> String.toInt
                                    |> Maybe.withDefault 0
                            )
            in
            { monkey | items = items }

        "Operation" :: rest ->
            let
                operationList =
                    rest
                        |> List.head
                        |> Maybe.withDefault ""
                        |> String.words
            in
            case operationList of
                "new" :: "=" :: left_ :: operation_ :: right_ :: [] ->
                    let
                        left : Operand
                        left =
                            getOperand left_

                        right : Operand
                        right =
                            getOperand right_

                        operation : Operation
                        operation =
                            getOperation operation_
                    in
                    { monkey | left = left, right = right, operation = operation }

                _ ->
                    monkey

        "Test" :: rest ->
            let
                divisibleByList : List String
                divisibleByList =
                    rest
                        |> List.head
                        |> Maybe.withDefault ""
                        |> String.words

                divisibleBy : Int
                divisibleBy =
                    case divisibleByList of
                        "divisible" :: "by" :: v :: [] ->
                            Maybe.withDefault 0 (String.toInt v)

                        _ ->
                            0
            in
            { monkey | divisibleBy = divisibleBy }

        "If true" :: rest ->
            let
                throw : Int
                throw =
                    getThrow (Maybe.withDefault "" (List.head rest))
            in
            { monkey | ifTrue = throw }

        "If false" :: rest ->
            let
                throw : Int
                throw =
                    getThrow (Maybe.withDefault "" (List.head rest))
            in
            { monkey | ifFalse = throw }

        _ ->
            monkey


getThrow : String -> Int
getThrow str =
    case String.words str of
        "throw" :: "to" :: "monkey" :: v :: [] ->
            Maybe.withDefault 0 (String.toInt v)

        _ ->
            0


getOperand : String -> Operand
getOperand str =
    case str of
        "old" ->
            Old

        _ ->
            New (Maybe.withDefault 0 (String.toInt str))


getOperation : String -> Operation
getOperation str =
    case str of
        "*" ->
            Multiplication

        _ ->
            Addition


logItems : Int -> Int -> Dict Int Monkey -> String
logItems index worry monkeys =
    let
        itemsString : String
        itemsString =
            monkeys
                |> Dict.toList
                |> List.foldl
                    (\( _, cur ) ( i, acc ) ->
                        ( i + 1
                        , acc
                            ++ "📜"
                            ++ Debug.toString i
                            ++ Debug.toString cur.items
                        )
                    )
                    ( 0, "" )
                |> Tuple.second
    in
    "🐵"
        ++ String.fromInt index
        ++ ": "
        ++ ", 😟"
        ++ String.fromInt worry
        ++ ", "
        ++ itemsString


round : Dict Int Monkey -> Dict Int Monkey
round monkeys =
    monkeys
        |> Dict.toList
        |> List.foldl handleMonkey monkeys


grabWorry : Int -> Dict Int Monkey -> ( Maybe Int, Dict Int Monkey )
grabWorry index monkeys =
    let
        monkey : Monkey
        monkey =
            Maybe.withDefault initialMonkey (Dict.get index monkeys)

        worry : Maybe Int
        worry =
            List.head monkey.items

        restItems : List Int
        restItems =
            Maybe.withDefault [] (List.tail monkey.items)

        updater x =
            case x of
                Just y ->
                    Just
                        { y | items = restItems }

                Nothing ->
                    Just initialMonkey
    in
    ( worry, Dict.update index updater monkeys )


handleMonkey : ( Int, Monkey ) -> Dict Int Monkey -> Dict Int Monkey
handleMonkey ( index, monkey ) monkeys =
    inspect ( index, monkeys )
        |> Tuple.second


inspect : ( Int, Dict Int Monkey ) -> ( Int, Dict Int Monkey )
inspect ( index, monkeys_ ) =
    let
        ( maybeWorry, monkeys ) =
            grabWorry index monkeys_

        monkey : Maybe Monkey
        monkey =
            Dict.get index monkeys
    in
    case maybeWorry of
        Just worry ->
            case monkey of
                Just m ->
                    let
                        newWorry : Int
                        newWorry =
                            calculateWorry worry m

                        throwTo : Int
                        throwTo =
                            testWorry newWorry m

                        newMonkeys : Dict Int Monkey
                        newMonkeys =
                            handleThrow worry newWorry index throwTo monkeys

                        _ =
                            Debug.log "Items" (logItems index newWorry newMonkeys)
                    in
                    inspect ( index, newMonkeys )

                Nothing ->
                    ( index, monkeys )

        Nothing ->
            ( index, monkeys )


calculateWorry : Int -> Monkey -> Int
calculateWorry worry monkey =
    let
        left : Int
        left =
            case monkey.left of
                Old ->
                    worry

                New n ->
                    n

        right : Int
        right =
            case monkey.right of
                Old ->
                    worry

                New n ->
                    n

        operation : Operation
        operation =
            monkey.operation
    in
    case operation of
        Multiplication ->
            floor (toFloat (left * right) / 3)

        Addition ->
            floor (toFloat (left + right) / 3)


testWorry : Int -> Monkey -> Int
testWorry worry monkey =
    let
        divisibleBy : Int
        divisibleBy =
            monkey.divisibleBy

        test : Bool
        test =
            modBy divisibleBy worry == 0
    in
    if test then
        monkey.ifTrue

    else
        monkey.ifFalse


handleThrow : Int -> Int -> Int -> Int -> Dict Int Monkey -> Dict Int Monkey
handleThrow initialWorry worry throwFrom throwTo monkeys =
    let
        monkeyFrom : Monkey
        monkeyFrom =
            Maybe.withDefault initialMonkey (Dict.get throwFrom monkeys)

        monkeyTo : Monkey
        monkeyTo =
            Maybe.withDefault initialMonkey (Dict.get throwTo monkeys)

        newFromItems : List Int
        newFromItems =
            monkeyFrom.items
                |> List.filter (\x -> x /= initialWorry)

        newToItems : List Int
        newToItems =
            List.append monkeyTo.items [ worry ]

        monkeysWithNewFromItems : Dict Int Monkey
        monkeysWithNewFromItems =
            Dict.update throwFrom
                (\x ->
                    case x of
                        Just y ->
                            Just
                                { y
                                    | items = newFromItems
                                    , itemsInspected = y.itemsInspected + 1
                                }

                        Nothing ->
                            Just initialMonkey
                )
                monkeys
    in
    Dict.update throwTo
        (\x ->
            case x of
                Just y ->
                    Just { y | items = newToItems }

                Nothing ->
                    Just initialMonkey
        )
        monkeysWithNewFromItems


solvePart1 : String -> ( Int, Dict Int Monkey )
solvePart1 input =
    input
        |> parseInput
        |> round
        |> round
        |> round
        |> round
        |> round
        |> round
        |> round
        |> round
        |> round
        |> round
        |> round
        |> round
        |> round
        |> round
        |> round
        |> round
        |> round
        |> round
        |> round
        |> round
        |> (\x ->
                let
                    answer : Int
                    answer =
                        x
                            |> Dict.toList
                            |> List.map Tuple.second
                            |> List.map .itemsInspected
                            |> List.sort
                            |> List.reverse
                            |> List.take 2
                            |> Debug.log "list"
                            |> List.foldl (*) 1

                    _ =
                        Debug.log "answer" answer
                in
                ( answer, x )
           )


solvePart2 : String -> Dict Int Monkey
solvePart2 input =
    input
        |> parseInput


part1 : Html msg
part1 =
    input
        |> solvePart1
        |> (\( answer, x ) ->
                viewTable answer (Dict.toList x)
           )


viewTable : Int -> List ( Int, Monkey ) -> Html msg
viewTable answer monkeys =
    table
        [ style "width" "100%"
        , style "font-size" "1rem"
        , style "text-align" "left"
        ]
        (tr [ style "color" "var(--c-red)" ]
            [ th [] [ text "🐵" ]
            , th [] [ text "Items" ]
            , th [] [ text "Operation" ]
            , th [] [ text "Items inspected" ]
            , th [] [ text "Answer" ]
            ]
            :: List.map (viewRow answer) monkeys
        )


viewRow : Int -> ( Int, Monkey ) -> Html msg
viewRow answer ( index, monkey ) =
    tr []
        [ td [] [ text (Debug.toString index) ]
        , td [] [ text (Debug.toString monkey.items) ]
        , td [] [ text (Debug.toString monkey.operation) ]
        , td [] [ text (Debug.toString monkey.itemsInspected) ]
        , td [] [ text (Debug.toString answer) ]
        ]


part2 : Html msg
part2 =
    input
        |> solvePart2
        |> Debug.toString
        |> Html.text
