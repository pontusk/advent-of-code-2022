module Days.Day16.Page exposing (..)

import Days.Day16.Inputs exposing (input)
import Html exposing (Html)
import Utils exposing (..)


parseInput : String -> String
parseInput input =
    input
       |> String.trim


solvePart1 : String -> String
solvePart1 input =
    input
        |> parseInput


solvePart2 : String -> String
solvePart2 input =
    input
        |> parseInput


part1 : Html msg
part1 =
    input
        |> solvePart1
        |> Debug.toString
        |> Html.text


part2 : Html msg
part2 =
    input
        |> solvePart2
        |> Debug.toString
        |> Html.text
