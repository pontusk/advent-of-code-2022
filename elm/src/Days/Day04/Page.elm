module Days.Day04.Page exposing (..)

import Days.Day04.Inputs exposing (finalInput)
import Html exposing (Html)
import List.Split exposing (..)
import Set exposing (Set)
import Utils exposing (..)


parseInput : String -> List (List (List Int))
parseInput input =
    input
        |> String.trim
        |> String.lines
        |> List.map
            (\x ->
                x
                    |> String.split ","
                    |> List.map
                        (\y ->
                            String.split "-" y
                                |> List.map
                                    (\z ->
                                        Maybe.withDefault 0 (String.toInt z)
                                    )
                        )
            )


toRanges : List (List Int) -> List (Set Int)
toRanges group =
    group
        |> List.map
            (\elf ->
                case elf of
                    from :: to :: [] ->
                        List.range from to |> Set.fromList

                    _ ->
                        Set.empty
            )


findSuperfluousGroup : List (Set Int) -> Int
findSuperfluousGroup group =
    case group of
        elf1 :: elf2 :: [] ->
            let
                diffLength1 =
                    Set.diff elf1 elf2 |> Set.toList |> List.length

                diffLenght2 =
                    Set.diff elf2 elf1 |> Set.toList |> List.length
            in
            if diffLength1 == 0 || diffLenght2 == 0 then
                1

            else
                0

        _ ->
            0


findIntersectingGroup : List (Set Int) -> Int
findIntersectingGroup group =
    case group of
        elf1 :: elf2 :: [] ->
            let
                intersectLength =
                    Set.intersect elf1 elf2
                        |> Set.toList
                        |> List.length
            in
            if intersectLength /= 0 then
                1

            else
                0

        _ ->
            0


solvePart1 : String -> Int
solvePart1 input =
    input
        |> parseInput
        |> List.map toRanges
        |> List.map findSuperfluousGroup
        |> List.sum


solvePart2 : String -> Int
solvePart2 input =
    input
        |> parseInput
        |> List.map toRanges
        |> List.map findIntersectingGroup
        |> List.sum


part1 : Html msg
part1 =
    finalInput
        |> solvePart1
        |> Debug.toString
        |> Html.text


part2 : Html msg
part2 =
    finalInput
        |> solvePart2
        |> Debug.toString
        |> Html.text
