module Days.Day02.Page exposing (..)

import Days.Day02.Inputs exposing (finalInput)
import Html exposing (Html)
import Utils exposing (..)


parseInput : String -> List (List Char)
parseInput input =
    input
        |> String.trim
        |> String.lines
        |> List.map
            (\x ->
                x
                    |> String.toList
                    |> List.filter Char.isAlphaNum
            )


solvePart1 : String -> Int
solvePart1 input =
    input
        |> parseInput
        |> List.foldl
            (\charList acc ->
                case charList of
                    'A' :: 'X' :: [] ->
                        acc + 1 + 3

                    'A' :: 'Y' :: [] ->
                        acc + 2 + 6

                    'A' :: 'Z' :: [] ->
                        acc + 3 + 0

                    'B' :: 'X' :: [] ->
                        acc + 1 + 0

                    'B' :: 'Y' :: [] ->
                        acc + 2 + 3

                    'B' :: 'Z' :: [] ->
                        acc + 3 + 6

                    'C' :: 'X' :: [] ->
                        acc + 1 + 6

                    'C' :: 'Y' :: [] ->
                        acc + 2 + 0

                    'C' :: 'Z' :: [] ->
                        acc + 3 + 3

                    _ ->
                        acc + 0
            )
            0



-- X loose 0
-- Y draw 3
-- Z win 6


solvePart2 : String -> Int
solvePart2 input =
    input
        |> parseInput
        |> List.foldl
            (\charList acc ->
                case charList of
                    'A' :: 'X' :: [] ->
                        acc + 3 + 0

                    'A' :: 'Y' :: [] ->
                        acc + 1 + 3

                    'A' :: 'Z' :: [] ->
                        acc + 2 + 6

                    'B' :: 'X' :: [] ->
                        acc + 1 + 0

                    'B' :: 'Y' :: [] ->
                        acc + 2 + 3

                    'B' :: 'Z' :: [] ->
                        acc + 3 + 6

                    'C' :: 'X' :: [] ->
                        acc + 2 + 0

                    'C' :: 'Y' :: [] ->
                        acc + 3 + 3

                    'C' :: 'Z' :: [] ->
                        acc + 1 + 6

                    _ ->
                        acc + 0
            )
            0


part1 : Html msg
part1 =
    finalInput
        |> solvePart1
        |> Debug.toString
        |> Html.text


part2 : Html msg
part2 =
    finalInput
        |> solvePart2
        |> Debug.toString
        |> Html.text
