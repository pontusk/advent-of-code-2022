module Days.Day08.Page exposing (..)

import Array exposing (Array)
import Days.Day08.Inputs exposing (..)
import Html exposing (Html)
import Utils exposing (..)


type alias Model =
    { treeMatrix : TreeMatrix
    , treeList : List Tree
    }


type alias TreeMatrix =
    Array (Array Int)


type alias Tree =
    { x : Int
    , y : Int
    , visible : Maybe Bool
    , vdTop : Int
    , vdRight : Int
    , vdBottom : Int
    , vdLeft : Int
    }


type alias Coords =
    ( Int, Int )


type alias CoordinateMods =
    ( Int, Int )


initialModel : Model
initialModel =
    { treeMatrix = Array.fromList [ Array.empty ]
    , treeList = []
    }


initialTree : Tree
initialTree =
    { x = 1
    , y = 1
    , visible = Nothing
    , vdTop = 0
    , vdRight = 0
    , vdBottom = 0
    , vdLeft = 0
    }


parseInput : String -> Model
parseInput input =
    input
        |> String.trim
        |> String.lines
        |> List.map
            (\l ->
                l
                    |> String.split ""
                    |> List.map (\n -> Maybe.withDefault 0 (String.toInt n))
                    |> Array.fromList
            )
        |> Array.fromList
        |> createModel


createModel : TreeMatrix -> Model
createModel treeMatrix =
    let
        trimEdges : Array a -> Array a
        trimEdges array =
            array
                |> Utils.removeFromArrayAt 0
                |> Utils.removeFromArrayLast

        assignCoords : Array Int -> ( Int, List Tree ) -> ( Int, List Tree )
        assignCoords array ( curY, list ) =
            let
                xRange : List Int
                xRange =
                    List.range 1 (Array.length array - 2)

                newList : List Tree
                newList =
                    xRange
                        |> List.map
                            (\x ->
                                { initialTree | x = x, y = curY }
                            )
            in
            ( curY + 1
            , newList ++ list
            )

        treeList : List Tree
        treeList =
            treeMatrix
                |> trimEdges
                |> Array.toList
                |> List.foldl assignCoords ( 1, [] )
                |> Tuple.second
    in
    { treeMatrix = treeMatrix, treeList = treeList }


getHeight : Tree -> TreeMatrix -> Maybe Int
getHeight tree treeMatrix =
    treeMatrix
        |> Array.get tree.y
        |> Maybe.andThen (\x -> Array.get tree.x x)


calculateVisibility : Model -> Model
calculateVisibility model =
    let
        checkDirection : CoordinateMods -> ( Coords, Tree ) -> ( Coords, Tree )
        checkDirection ( xMod, yMod ) ( acc, tree ) =
            let
                maybeHeight : Maybe Int
                maybeHeight =
                    getHeight tree model.treeMatrix

                ( accX, accY ) =
                    acc

                x =
                    accX + xMod

                y =
                    accY + yMod

                maybeCompareHeight : Maybe Int
                maybeCompareHeight =
                    getHeight { initialTree | x = x, y = y } model.treeMatrix
            in
            case maybeHeight of
                Just height ->
                    case maybeCompareHeight of
                        Just compareHeight ->
                            let
                                visible : Bool
                                visible =
                                    height > compareHeight
                            in
                            case tree.visible of
                                Just False ->
                                    checkDirection ( xMod, yMod ) ( ( x, y ), tree )

                                _ ->
                                    checkDirection ( xMod, yMod ) ( ( x, y ), { tree | visible = Just visible } )

                        Nothing ->
                            ( acc, tree )

                Nothing ->
                    ( acc, tree )

        checkTop : ( Coords, Tree ) -> ( Coords, Tree )
        checkTop =
            checkDirection ( 0, -1 )

        checkRight : ( Coords, Tree ) -> ( Coords, Tree )
        checkRight =
            checkDirection ( 1, 0 )

        checkBottom : ( Coords, Tree ) -> ( Coords, Tree )
        checkBottom =
            checkDirection ( 0, 1 )

        checkLeft : ( Coords, Tree ) -> ( Coords, Tree )
        checkLeft =
            checkDirection ( -1, 0 )

        checkDirections : Tree -> Tree
        checkDirections tree =
            let
                visibleFromTop =
                    Maybe.withDefault False
                        (checkTop ( ( tree.x, tree.y ), tree )
                            |> Tuple.second
                            |> .visible
                        )

                visibleFromRight =
                    Maybe.withDefault False
                        (checkRight ( ( tree.x, tree.y ), tree )
                            |> Tuple.second
                            |> .visible
                        )

                visibleFromBottom =
                    Maybe.withDefault False
                        (checkBottom ( ( tree.x, tree.y ), tree )
                            |> Tuple.second
                            |> .visible
                        )

                visibleFromLeft =
                    Maybe.withDefault False
                        (checkLeft ( ( tree.x, tree.y ), tree )
                            |> Tuple.second
                            |> .visible
                        )

                isVisible =
                    visibleFromTop
                        || visibleFromRight
                        || visibleFromBottom
                        || visibleFromLeft
            in
            { tree | visible = Just isVisible }

        newTrees : List Tree
        newTrees =
            model.treeList
                |> List.map checkDirections
    in
    { model | treeList = newTrees }


type Direction
    = Top
    | Right
    | Bottom
    | Left


calculateViewingDistance : Model -> Model
calculateViewingDistance model =
    let
        checkDirection : Direction -> ( Coords, Tree ) -> ( Coords, Tree )
        checkDirection direction ( acc, tree ) =
            let
                maybeHeight : Maybe Int
                maybeHeight =
                    getHeight tree model.treeMatrix

                ( accX, accY ) =
                    acc

                ( xMod, yMod ) =
                    case direction of
                        Top ->
                            ( 0, -1 )

                        Right ->
                            ( 1, 0 )

                        Bottom ->
                            ( 0, 1 )

                        Left ->
                            ( -1, 0 )

                x =
                    accX + xMod

                y =
                    accY + yMod

                maybeCompareHeight : Maybe Int
                maybeCompareHeight =
                    getHeight { initialTree | x = x, y = y } model.treeMatrix
            in
            case maybeHeight of
                Just height ->
                    case maybeCompareHeight of
                        Just compareHeight ->
                            let
                                visible : Bool
                                visible =
                                    height > compareHeight
                            in
                            if visible then
                                case direction of
                                    Top ->
                                        checkDirection direction ( ( x, y ), { tree | visible = Just visible, vdTop = tree.vdTop + 1 } )

                                    Right ->
                                        checkDirection direction ( ( x, y ), { tree | visible = Just visible, vdRight = tree.vdRight + 1 } )

                                    Bottom ->
                                        checkDirection direction ( ( x, y ), { tree | visible = Just visible, vdBottom = tree.vdBottom + 1 } )

                                    Left ->
                                        checkDirection direction ( ( x, y ), { tree | visible = Just visible, vdLeft = tree.vdLeft + 1 } )

                            else
                                case direction of
                                    Top ->
                                        ( ( x, y ), { tree | visible = Just visible, vdTop = tree.vdTop + 1 } )

                                    Left ->
                                        ( ( x, y ), { tree | visible = Just visible, vdLeft = tree.vdLeft + 1 } )

                                    Right ->
                                        ( ( x, y ), { tree | visible = Just visible, vdRight = tree.vdRight + 1 } )

                                    Bottom ->
                                        ( ( x, y ), { tree | visible = Just visible, vdBottom = tree.vdBottom + 1 } )

                        Nothing ->
                            ( acc, tree )

                Nothing ->
                    ( acc, tree )

        checkTop : ( Coords, Tree ) -> ( Coords, Tree )
        checkTop =
            checkDirection Top

        checkRight : ( Coords, Tree ) -> ( Coords, Tree )
        checkRight =
            checkDirection Right

        checkBottom : ( Coords, Tree ) -> ( Coords, Tree )
        checkBottom =
            checkDirection Bottom

        checkLeft : ( Coords, Tree ) -> ( Coords, Tree )
        checkLeft =
            checkDirection Left

        reset : ( Coords, Tree ) -> ( Coords, Tree )
        reset ( coords, tree ) =
            ( ( tree.x, tree.y ), tree )

        checkDirections : Tree -> Tree
        checkDirections tree =
            tree
                |> (\t -> ( ( t.x, t.y ), t ))
                |> checkTop
                |> reset
                |> checkRight
                |> reset
                |> checkBottom
                |> reset
                |> checkLeft
                |> reset
                |> Tuple.second

        newTrees : List Tree
        newTrees =
            model.treeList
                |> List.map checkDirections
    in
    { model | treeList = newTrees }


countVisible : Model -> Int
countVisible model =
    model.treeList
        |> List.filter
            (\tree ->
                case tree.visible of
                    Just True ->
                        True

                    _ ->
                        False
            )
        |> List.length
        |> (\acc ->
                let
                    matrixHeight : Int
                    matrixHeight =
                        Array.length model.treeMatrix

                    firstRow : Array Int
                    firstRow =
                        Maybe.withDefault Array.empty (Array.get 0 model.treeMatrix)

                    matrixWidth : Int
                    matrixWidth =
                        Array.length firstRow
                in
                acc + matrixHeight * 2 + matrixWidth * 2 - 4
           )


countViewingDistances : Model -> Int
countViewingDistances model =
    model.treeList
        |> List.map
            (\l ->
                l.vdTop * l.vdRight * l.vdBottom * l.vdLeft
            )
        |> List.maximum
        |> Maybe.withDefault 0


solvePart1 : String -> Int
solvePart1 input =
    input
        |> parseInput
        |> calculateVisibility
        |> countVisible


solvePart2 : String -> Int
solvePart2 input =
    input
        |> parseInput
        |> calculateViewingDistance
        |> countViewingDistances


part1 : Html msg
part1 =
    finalInput
        |> solvePart1
        |> Debug.toString
        |> Html.text


part2 : Html msg
part2 =
    finalInput
        |> solvePart2
        |> Debug.toString
        |> Html.text
