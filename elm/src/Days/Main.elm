module Days.Main exposing (viewContent)

import Days.Day01.Page as Day1 exposing (..)
import Days.Day02.Page as Day2 exposing (..)
import Days.Day03.Page as Day3 exposing (..)
import Days.Day04.Page as Day4 exposing (..)
import Days.Day05.Page as Day5 exposing (..)
import Days.Day06.Page as Day6 exposing (..)
import Days.Day07.Page as Day7 exposing (..)
import Days.Day08.Page as Day8 exposing (..)
import Days.Day09.Page as Day9 exposing (..)
import Days.Day10.Page as Day10 exposing (..)
import Days.Day11.Page as Day11 exposing (..)
import Days.Day12.Page as Day12 exposing (..)
import Days.Day13.Page as Day13 exposing (..)
import Days.Day14.Page as Day14 exposing (..)
import Days.Day15.Page as Day15 exposing (..)
import Days.Day16.Page as Day16 exposing (..)
import Days.Day17.Page as Day17 exposing (..)
import Days.Day18.Page as Day18 exposing (..)
import Days.Day19.Page as Day19 exposing (..)
import Days.Day20.Page as Day20 exposing (..)
import Days.Day21.Page as Day21 exposing (..)
import Days.Day22.Page as Day22 exposing (..)
import Days.Day23.Page as Day23 exposing (..)
import Days.Day24.Page as Day24 exposing (..)
import Days.Day25.Page as Day25 exposing (..)
import Types exposing (Model, Msg(..))
import Html exposing (Html)
import Html exposing (div)


viewContent : Model -> Html Msg
viewContent model =
    case model.day of
        1 ->
            if model.part == 2 then
                Day1.part2

            else
                Day1.part1

        2 ->
            if model.part == 2 then
                Day2.part2

            else
                Day2.part1

        3 ->
            if model.part == 2 then
                Day3.part2

            else
                Day3.part1

        4 ->
            if model.part == 2 then
                Day4.part2

            else
                Day4.part1

        5 ->
            if model.part == 2 then
                Day5.part2

            else
                Day5.part1

        6 ->
            if model.part == 2 then
                Day6.part2

            else
                Day6.part1

        7 ->
            if model.part == 2 then
                Day7.part2

            else
                Day7.part1

        8 ->
            if model.part == 2 then
                Day8.part2

            else
                Day8.part1

        9 ->
            if model.part == 2 then
                Day9.part2

            else
                Day9.part1

        10 ->
            if model.part == 2 then
                Day10.part2

            else
                Day10.part1

        11 ->
            if model.part == 2 then
                Day11.part2

            else
                Day11.part1

        12 ->
            if model.part == 2 then
                Day12.part2

            else
                Day12.part1

        13 ->
            if model.part == 2 then
                Day13.part2

            else
                Day13.part1

        14 ->
            if model.part == 2 then
                Day14.part2

            else
                Day14.part1

        15 ->
            if model.part == 2 then
                Day15.part2

            else
                Day15.part1

        16 ->
            if model.part == 2 then
                Day16.part2

            else
                Day16.part1

        17 ->
            if model.part == 2 then
                Day17.part2

            else
                Day17.part1

        18 ->
            if model.part == 2 then
                Day18.part2

            else
                Day18.part1

        19 ->
            if model.part == 2 then
                Day19.part2

            else
                Day19.part1

        20 ->
            if model.part == 2 then
                Day20.part2

            else
                Day20.part1

        21 ->
            if model.part == 2 then
                Day21.part2

            else
                Day21.part1

        22 ->
            if model.part == 2 then
                Day22.part2

            else
                Day22.part1

        23 ->
            if model.part == 2 then
                Day23.part2

            else
                Day23.part1

        24 ->
            if model.part == 2 then
                Day24.part2

            else
                Day24.part1

        25 ->
            if model.part == 2 then
                Day25.part2

            else
                Day25.part1

        _ ->
            div [] []
