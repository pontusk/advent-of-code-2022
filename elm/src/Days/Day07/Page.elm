module Days.Day07.Page exposing (..)

import Days.Day07.Inputs exposing (..)
import Dict exposing (Dict)
import Html exposing (Html)
import Set exposing (Set)
import Utils exposing (..)


type FileType
    = Dir
    | File


type Cmd
    = Cd NodeInfo
    | CdBack
    | MkDir NodeInfo
    | TouchFile NodeInfo
    | Ls


type alias NodeInfo =
    { path : String
    , type_ : FileType
    , size : Int
    }


type alias Node =
    { path : String
    , type_ : FileType
    , size : Int
    , content : Set String
    }


type alias Model =
    { fileSystem : Dict String Node
    , trail : List String
    }


initialModel : Model
initialModel =
    { fileSystem = Dict.empty
    , trail = []
    }


parseInput : String -> List String
parseInput input =
    input
        |> String.trim
        |> String.lines


identifyCmd : String -> Maybe Cmd
identifyCmd line =
    let
        getPathName : String -> String
        getPathName name =
            if name == "/" then
                "/"

            else
                name ++ "/"
    in
    case String.words line of
        "$" :: "cd" :: ".." :: [] ->
            Just CdBack

        "$" :: "cd" :: name :: [] ->
            Just (Cd { path = getPathName name, size = 0, type_ = Dir })

        "$" :: "ls" :: [] ->
            Just Ls

        "dir" :: name :: [] ->
            Just (MkDir { path = getPathName name, size = 0, type_ = Dir })

        maybeSize :: name :: [] ->
            case String.toInt maybeSize of
                Just s ->
                    Just (TouchFile { path = getPathName name, size = s, type_ = File })

                _ ->
                    Nothing

        _ ->
            Nothing


pathFromTrail : String -> String -> String
pathFromTrail name acc =
    name ++ acc


executeCmd : Cmd -> Model -> Model
executeCmd cmd model =
    case cmd of
        Cd info ->
            let
                trail =
                    info.path :: model.trail

                path =
                    List.foldl pathFromTrail "" trail
            in
            { model
                | trail = trail
                , fileSystem = updateFileSystem { info | path = path } model.fileSystem
            }

        CdBack ->
            { model | trail = Maybe.withDefault [] (List.tail model.trail) }

        MkDir info ->
            let
                trail =
                    info.path :: model.trail

                path =
                    List.foldl pathFromTrail "" trail

                parentpath : String
                parentpath =
                    List.foldl pathFromTrail "" model.trail

                fileSystemWithUpdatedContent : Dict String Node
                fileSystemWithUpdatedContent =
                    updateContent parentpath { info | path = path } model.fileSystem
            in
            { model
                | fileSystem =
                    updateFileSystem { info | path = path } fileSystemWithUpdatedContent
            }

        TouchFile info ->
            let
                trail =
                    info.path :: model.trail

                path =
                    List.foldl pathFromTrail "" trail

                parentpath : String
                parentpath =
                    List.foldl pathFromTrail "" model.trail

                fileSystemWithUpdatedContent : Dict String Node
                fileSystemWithUpdatedContent =
                    updateContent parentpath { info | path = path } model.fileSystem
            in
            { model
                | fileSystem =
                    updateFileSystem { info | path = path } fileSystemWithUpdatedContent
            }

        _ ->
            model


updateFileSystem : NodeInfo -> Dict String Node -> Dict String Node
updateFileSystem info fileSystem =
    let
        existingValue : Maybe Node
        existingValue =
            Dict.get info.path fileSystem
    in
    case existingValue of
        Just _ ->
            Dict.update info.path
                (Maybe.map
                    (\n ->
                        { n
                            | size = info.size
                        }
                    )
                )
                fileSystem

        Nothing ->
            Dict.insert info.path
                { path = info.path
                , size = info.size
                , type_ = info.type_
                , content = Set.fromList []
                }
                fileSystem


updateContent : String -> NodeInfo -> Dict String Node -> Dict String Node
updateContent parentpath content fileSystem =
    let
        parent : Maybe Node
        parent =
            Dict.get parentpath fileSystem
    in
    case parent of
        Just parentNode ->
            Dict.update parentNode.path
                (Maybe.map
                    (\n ->
                        { n
                            | content = Set.insert content.path n.content
                        }
                    )
                )
                fileSystem

        Nothing ->
            fileSystem


fileSystemToContent : Dict String Node -> List String
fileSystemToContent fileSystem =
    fileSystem
        |> Dict.toList
        |> List.map
            (\x ->
                x |> Tuple.second |> .path
            )


calculateSizes : FileType -> Model -> Model
calculateSizes type_ model =
    Dict.keys model.fileSystem
        |> List.sortBy String.length
        |> List.foldr
            (\cur acc ->
                let
                    maybeNode : Maybe Node
                    maybeNode =
                        Dict.get cur acc.fileSystem
                in
                case maybeNode of
                    Just node ->
                        if node.type_ == File then
                            acc

                        else
                            let
                                fileSizeSum =
                                    node.content
                                        |> Set.foldl
                                            (\x a ->
                                                let
                                                    mbn =
                                                        Dict.get x acc.fileSystem
                                                in
                                                case mbn of
                                                    Just n ->
                                                        if n.type_ == type_ then
                                                            a + n.size

                                                        else
                                                            a

                                                    Nothing ->
                                                        a
                                            )
                                            0

                                newFileSystem =
                                    updateFileSystem
                                        { path = node.path
                                        , type_ = node.type_
                                        , size = node.size + fileSizeSum 
                                        }
                                        acc.fileSystem
                            in
                            { acc | fileSystem = newFileSystem }

                    Nothing ->
                        acc
            )
            model




findFolderToDelete : Dict String Node -> Int
findFolderToDelete fileSystem =
    let
        rootFolder : Maybe Node
        rootFolder =
            Dict.get "/" fileSystem
    in
    case rootFolder of
        Just folder ->
            let
                spaceLeft : Int
                spaceLeft =
                    70000000 - folder.size

                needToSave : Int
                needToSave =
                    30000000 - spaceLeft

            in
            if needToSave < 0 then
                0

            else
                let
                    maybeMax : Maybe Int
                    maybeMax =
                        fileSystem
                            |> Dict.values
                            |> List.filter (\node -> node.type_ == Dir)
                            |> List.map .size
                            |> List.filter (\s -> s > needToSave)
                            |> List.minimum
                in
                case maybeMax of
                    Just max ->
                        max

                    Nothing ->
                        0

        Nothing ->
            0


solvePart1 : String -> Int
solvePart1 input =
    input
        |> parseInput
        |> List.filterMap identifyCmd
        |> List.foldl executeCmd initialModel
        |> calculateSizes File 
        |> calculateSizes Dir
        |> .fileSystem
        |> Dict.values
        |> List.filter (\node -> node.type_ == Dir)
        |> List.map .size
        |> List.filter (\x -> x <= 100000)
        |> List.sum


solvePart2 : String -> Int
solvePart2 input =
    input
        |> parseInput
        |> List.filterMap identifyCmd
        |> List.foldl executeCmd initialModel
        |> calculateSizes File
        |> calculateSizes Dir
        |> .fileSystem
        |> findFolderToDelete


part1 : Html msg
part1 =
    finalInput
        |> solvePart1
        |> Debug.toString
        |> Html.text


part2 : Html msg
part2 =
    finalInput
        |> solvePart2
        |> Debug.toString
        |> Html.text
