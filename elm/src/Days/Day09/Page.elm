module Days.Day09.Page exposing (..)

import Array exposing (Array)
import Days.Day09.Inputs exposing (finalInput, input)
import Html exposing (Html)
import Set exposing (Set)
import Utils exposing (..)


type Instruction
    = Up Int
    | Right Int
    | Down Int
    | Left Int


type alias LongTail =
    { one : Vector
    , two : Vector
    , three : Vector
    , four : Vector
    , five : Vector
    , six : Vector
    , seven : Vector
    , eight : Vector
    , nine : Vector
    }


type alias Model =
    { instructions : Array Instruction
    , index : Int
    , start : Vector
    , head : Vector
    , tail : Vector
    , longTail : LongTail
    , visited : Set Vector
    , longTailVisited : Set Vector
    }


type alias Vector =
    ( Int, Int )


type RelativePosition
    = StraightUp
    | DiagonalUpLeft
    | StraightLeft
    | DiagonalDownLeft
    | StraightDown
    | DiagonalDownRight
    | StraightRight
    | DiagonalUpRight
    | Unchanged


initialModel : Model
initialModel =
    { instructions = Array.fromList []
    , index = 0
    , start = ( 0, 0 )
    , head = ( 0, 0 )
    , tail = ( 0, 0 )
    , longTail =
        { one = ( 0, 0 )
        , two = ( 0, 0 )
        , three = ( 0, 0 )
        , four = ( 0, 0 )
        , five = ( 0, 0 )
        , six = ( 0, 0 )
        , seven = ( 0, 0 )
        , eight = ( 0, 0 )
        , nine = ( 0, 0 )
        }
    , visited = Set.fromList [ ( 0, 0 ) ]
    , longTailVisited = Set.fromList [ ( 0, 0 ) ]
    }


parseInput : String -> Model
parseInput input =
    input
        |> String.trim
        |> String.lines
        |> List.filterMap
            (\x ->
                let
                    getNum : String -> Int
                    getNum str =
                        Maybe.withDefault 0 (String.toInt str)
                in
                case String.words x of
                    "U" :: str :: [] ->
                        Just (Up (getNum str))

                    "R" :: str :: [] ->
                        Just (Right (getNum str))

                    "D" :: str :: [] ->
                        Just (Down (getNum str))

                    "L" :: str :: [] ->
                        Just (Left (getNum str))

                    _ ->
                        Nothing
            )
        |> Array.fromList
        |> (\x -> { initialModel | instructions = x })


step : Model -> Model
step model =
    let
        maybeInstruction : Maybe Instruction
        maybeInstruction =
            Array.get model.index model.instructions
    in
    case maybeInstruction of
        Just instruction ->
            let
                -- _ =
                --     Debug.log "instruction" instruction

                newModel : Model
                newModel =
                    executeInstruction instruction model
            in
            { newModel | index = model.index + 1 }

        Nothing ->
            model


executeInstruction : Instruction -> Model -> Model
executeInstruction instruction model =
    case instruction of
        Up steps ->
            movePositions steps (\( x, y ) n -> ( x, y - n )) model

        Right steps ->
            movePositions steps (\( x, y ) n -> ( x + n, y )) model

        Down steps ->
            movePositions steps (\( x, y ) n -> ( x, y + n )) model

        Left steps ->
            movePositions steps (\( x, y ) n -> ( x - n, y )) model


movePositions : Int -> (Vector -> Int -> Vector) -> Model -> Model
movePositions steps moveHead model =
    List.range 1 steps
        |> List.foldl
            (\_ acc ->
                let
                    newHead : Vector
                    newHead =
                        moveHead acc.head 1

                    newTail : Vector
                    newTail =
                        getTailPosition newHead acc.tail

                    newLongTail : LongTail
                    newLongTail =
                        getLongTailPosition newHead acc.longTail

                    newVisited =
                        Set.insert newTail acc.visited

                    newLongTailVisited =
                        Set.insert newLongTail.nine acc.longTailVisited

                    -- _ =
                    --     plot newHead newTail newLongTail
                in
                { acc
                    | head = newHead
                    , tail = newTail
                    , longTail = newLongTail
                    , visited = newVisited
                    , longTailVisited = newLongTailVisited
                }
            )
            model


getLongTailPosition : Vector -> LongTail -> LongTail
getLongTailPosition head longTail =
    let
        newOne : Vector
        newOne =
            getTailPosition head longTail.one

        newTwo : Vector
        newTwo =
            getTailPosition newOne longTail.two

        newThree : Vector
        newThree =
            getTailPosition newTwo longTail.three

        newFour : Vector
        newFour =
            getTailPosition newThree longTail.four

        newFive : Vector
        newFive =
            getTailPosition newFour longTail.five

        newSix : Vector
        newSix =
            getTailPosition newFive longTail.six

        newSeven : Vector
        newSeven =
            getTailPosition newSix longTail.seven

        newEight : Vector
        newEight =
            getTailPosition newSeven longTail.eight

        newNine : Vector
        newNine =
            getTailPosition newEight longTail.nine
    in
    { longTail
        | one = newOne
        , two = newTwo
        , three = newThree
        , four = newFour
        , five = newFive
        , six = newSix
        , seven = newSeven
        , eight = newEight
        , nine = newNine
    }


plot : Vector -> Vector -> LongTail -> List String
plot head tail longTail =
    let
        vectors : List Vector
        vectors =
            [ head
            , tail
            , longTail.one
            , longTail.two
            , longTail.three
            , longTail.four
            , longTail.five
            , longTail.six
            , longTail.seven
            , longTail.eight
            , longTail.nine
            ]

        maxX =
            vectors
                |> List.map Tuple.first
                |> List.map abs
                |> List.maximum
                |> Maybe.withDefault 0

        maxY =
            vectors
                |> List.map Tuple.second
                |> List.map abs
                |> List.maximum
                |> Maybe.withDefault 0
    in
    List.range 0 maxY
        |> List.reverse
        |> List.indexedMap
            (\_ yIndex ->
                List.range 0 maxX
                    |> List.indexedMap
                        (\_ xIndex ->
                            let
                                checkPosition : Vector -> Bool
                                checkPosition ( x, y ) =
                                    xIndex == abs x && yIndex == abs y
                            in
                            if checkPosition head then
                                'H'

                            else if checkPosition longTail.one then
                                '1'

                            else if checkPosition longTail.two then
                                '2'

                            else if checkPosition longTail.three then
                                '3'

                            else if checkPosition longTail.four then
                                '4'

                            else if checkPosition longTail.five then
                                '5'

                            else if checkPosition longTail.six then
                                '6'

                            else if checkPosition longTail.seven then
                                '7'

                            else if checkPosition longTail.eight then
                                '8'

                            else if checkPosition longTail.nine then
                                '9'

                            else if checkPosition tail then
                                'T'

                            else
                                '.'
                        )
                    |> String.fromList
                    |> Debug.log ""
            )


getTailPosition : Vector -> Vector -> Vector
getTailPosition head tail =
    let
        ( headX, headY ) =
            head

        ( tailX, tailY ) =
            tail

        ( deltaX, deltaY ) =
            ( headX - tailX, headY - tailY )

        relativePosition : RelativePosition
        relativePosition =
            case ( compare headX tailX, compare headY tailY ) of
                ( EQ, LT ) ->
                    StraightUp

                ( GT, LT ) ->
                    DiagonalUpRight

                ( GT, EQ ) ->
                    StraightRight

                ( GT, GT ) ->
                    DiagonalDownRight

                ( EQ, GT ) ->
                    StraightDown

                ( LT, GT ) ->
                    DiagonalDownLeft

                ( LT, EQ ) ->
                    StraightLeft

                ( LT, LT ) ->
                    DiagonalUpLeft

                ( EQ, EQ ) ->
                    Unchanged

        followHeadXPlus : Int
        followHeadXPlus =
            max tailX (tailX + deltaX - 1)

        followHeadXMinus : Int
        followHeadXMinus =
            min tailX (tailX + deltaX + 1)

        followHeadYPlus : Int
        followHeadYPlus =
            max tailY (tailY + deltaY - 1)

        followHeadYMinus : Int
        followHeadYMinus =
            min tailY (tailY + deltaY + 1)
    in
    case relativePosition of
        StraightUp ->
            ( tailX, followHeadYMinus )

        DiagonalUpRight ->
            if abs deltaX > 1 then
                ( followHeadXPlus, tailY - 1 )

            else if abs deltaY > 1 then
                ( tailX + 1, followHeadYMinus )

            else
                ( tailX, tailY )

        StraightRight ->
            ( followHeadXPlus, tailY )

        DiagonalDownRight ->
            if abs deltaX > 1 then
                ( followHeadXPlus, tailY + 1 )

            else if abs deltaY > 1 then
                ( tailX + 1, followHeadYPlus )

            else
                ( tailX, tailY )

        StraightDown ->
            ( tailX, followHeadYPlus )

        DiagonalDownLeft ->
            if abs deltaX > 1 then
                ( followHeadXMinus, tailY + 1 )

            else if abs deltaY > 1 then
                ( tailX - 1, followHeadYPlus )

            else
                ( tailX, tailY )

        StraightLeft ->
            ( followHeadXMinus, tailY )

        DiagonalUpLeft ->
            if abs deltaX > 1 then
                ( followHeadXMinus, tailY - 1 )

            else if abs deltaY > 1 then
                ( tailX - 1, followHeadYMinus )

            else
                ( tailX, tailY )

        Unchanged ->
            ( tailX, tailY )


solvePart1 : String -> Int
solvePart1 input =
    input
        |> parseInput
        |> (\model ->
                model.instructions
                    |> Array.toList
                    |> List.foldl
                        (\_ acc ->
                            step acc
                        )
                        model
           )
        |> .visited
        |> Set.size


solvePart2 : String -> Int
solvePart2 input =
    input
        |> parseInput
        |> (\model ->
                model.instructions
                    |> Array.toList
                    |> List.foldl
                        (\_ acc ->
                            step acc
                        )
                        model
           )
        |> .longTailVisited
        |> Set.size


part1 : Html msg
part1 =
    input
        |> solvePart1
        |> Debug.toString
        |> Html.text


part2 : Html msg
part2 =
    input
        |> solvePart2
        |> Debug.toString
        |> Html.text
