module Types exposing (Model, Msg(..), initialModel)


type alias Model =
    { day : Int
    , part : Int
    }


initialModel : Model
initialModel =
    { day = 1
    , part = 1
    }


type Msg
    = SetPage Int
    | SetPart Int
