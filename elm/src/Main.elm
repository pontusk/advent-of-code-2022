module Main exposing (main)

import Browser
import Days.Main exposing (viewContent)
import Html
    exposing
        ( Html
        , div
        , h1
        , h2
        , node
        , text
        )
import Html.Attributes exposing (class)
import Styles exposing (css)
import Types exposing (Model, Msg(..), initialModel)


day : Int
day =
    11


part : Int
part =
    1


update : Msg -> Model -> Model
update msg model =
    case msg of
        SetPage d ->
            { model | day = d }

        SetPart p ->
            { model | part = p }


view : Model -> Html Msg
view model =
    div []
        [ h1 [] [ text "Advent Of Code 2022" ]
        , h2 [] [ text ("Day " ++ String.fromInt day ++ ", Part " ++ String.fromInt part) ]
        , div [ class "frame" ]
            [ div [ class "content" ]
                [ viewContent
                    { model
                        | day = day
                        , part = part
                    }
                ]
            ]
        , node "style" [] [ text css ]
        ]


main : Program () Model Msg
main =
    Browser.sandbox
        { init = initialModel
        , view = view
        , update = update
        }
