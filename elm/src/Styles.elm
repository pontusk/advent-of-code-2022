module Styles exposing (..)


css : String
css =
    """
@import url('https://fonts.googleapis.com/css2?family=Comic+Neue:wght@700&family=Space+Mono&display=swap');

:root {
    --h1-size: 3rem;
    --h2-size: 1.75rem;
    --text-size: 1.2rem;
    --c-red: #ee5952;
    --c-black: #282828;
    --s-medium: 3.5rem;
    --s-small: 1.5rem;
}
h1, h2 {
    text-align: center;
}
h1 {
    font-weight: bold;
    font-size: var(--h1-size);
    color: var(--c-red);
    text-shadow:
      3px  3px 0 var(--c-black),  
     -1px -1px 0 var(--c-black),  
      1px -1px 0 var(--c-black),
     -1px  1px 0 var(--c-black),
      1px  1px 0 var(--c-black);
}
h2 {
    font-size: var(--h2-size);
}
body {
    font-family: 'Comic Neue', cursive;
    background-color: palegoldenrodseagreen;
    background: linear-gradient(yellowgreen, seagreen);
    background-repeat: no-repeat;
    background-size: cover;
    height: 100vh;
}
.frame {
    height: calc(80vh - var(--h1-size) - var(--h2-size) - (var(--s-medium)*2));
    margin: var(--s-medium) 10vw;
    display: flex;
    background-color: #282828;
    border-image: url(https://www.dropbox.com/s/y1m4yok5l9032xy/chrismasborder.png?raw=1) 30;
    border-image-slice: 260 240 260 283;
    border-image-width: 60px;
    border-image-outset: 20px;
    border-image-repeat: repeat;
}
.content {
    overflow: scroll;
    margin: var(--s-small) var(--s-medium);
    flex: 1;
    display: flex;
    justify-content: center;
    color: #ebdbb2;
    font-size: var(--text-size);
    font-family: 'Space Mono', monospace;
}
"""
