module Utils exposing (..)

import Array exposing (Array)


stringToInt : String -> Int
stringToInt str =
    Maybe.withDefault 0 (String.toInt str)


divideBy : Int -> Int -> Float
divideBy a b =
    toFloat b / toFloat a


subtract : Int -> Int -> Int
subtract a b =
    b - a


getAtIndex : Int -> Array Int -> Int
getAtIndex index arr =
    Maybe.withDefault 0 (Array.get index arr)


removeFromArrayAt : Int -> Array a -> Array a
removeFromArrayAt i a =
    let
        a1 =
            Array.slice 0 i a

        a2 =
            Array.slice (i + 1) (Array.length a) a
    in
    Array.append a1 a2


removeFromArrayLast : Array a -> Array a
removeFromArrayLast a =
    let
        last =
            Array.length a - 1
    in
    removeFromArrayAt last a
